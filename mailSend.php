<?php
include("phpMailer/class.phpmailer.php");
include("phpMailer/class.smtp.php"); // note, this is optional - gets called from main class if not already loaded

function mailSend($email, $name, $lastname, $title, $altBody, $body){

    $mail             = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPAuth   = true;                  // enable SMTP authentication
    $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
    $mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
    $mail->Port       = 465;                   // set the SMTP port

    $mail->Username   = "scfAdministrator@gmail.com";  // GMAIL username
    $mail->Password   = "zavrsniRad";            // GMAIL password
    $mail->SMTPKeepAlive = true;

    $mail->From       = "scfAdministrator@gmail.com";
    $mail->FromName   = "Administrator";
    $mail->Subject    = $title;
    $mail->AltBody    = $altBody;

    $mail->WordWrap   = 50; // set word wrap

    $mail->MsgHTML($body);

    $mail->AddAddress("$email","$name $lastname");

    $mail->IsHTML(true); // send as HTML
    $mail->CharSet = 'utf-8';
    //$mail->SMTPDebug =1;
    $SMTPDebug=false;
    if(!$mail->Send()) {
      return false;
    } else {
      return true;
    }
}
?>