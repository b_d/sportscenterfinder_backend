-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Računalo: localhost
-- Vrijeme generiranja: Kol 31, 2014 u 02:31 PM
-- Verzija poslužitelja: 5.1.61
-- PHP verzija: 5.3.3-7+squeeze9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza podataka: `SCF2`
--

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` text NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT AUTO_INCREMENT=12 ;

--
-- Izbacivanje podataka za tablicu `activities`
--

INSERT INTO `activities` (`activity_id`, `activity`) VALUES
(1, 'tenis'),
(2, 'teretana'),
(3, 'košarka'),
(4, 'nogomet'),
(5, 'biciklizam'),
(6, 'ultimate fight'),
(7, 'boks'),
(8, 'plivanje'),
(9, 'fitness'),
(10, 'snowboard'),
(11, 'mačevanje');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `available_activities`
--

CREATE TABLE IF NOT EXISTS `available_activities` (
  `center_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Izbacivanje podataka za tablicu `available_activities`
--

INSERT INTO `available_activities` (`center_id`, `activity_id`) VALUES
(55, 1),
(55, 6),
(55, 4),
(58, 1),
(59, 6),
(60, 7),
(61, 8),
(62, 10),
(63, 4),
(63, 11),
(64, 4),
(65, 1),
(66, 1),
(67, 4),
(68, 11),
(69, 10),
(70, 11),
(71, 1),
(72, 8),
(73, 4),
(111, 1),
(57, 9),
(57, 8),
(57, 4),
(57, 2),
(57, 1);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_center_id` int(11) NOT NULL,
  `comment` text CHARACTER SET utf8 NOT NULL,
  `time` text NOT NULL,
  `by_user` char(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Izbacivanje podataka za tablicu `comments`
--

INSERT INTO `comments` (`comment_id`, `sport_center_id`, `comment`, `time`, `by_user`) VALUES
(16, 55, 'Upisujem ovaj komentar kako bih nešto komentirao! Komentar je uspješno  izmijenjen!', '2012-07-06 09:49:22', 'admin'),
(17, 56, 'Komentiram ovaj centar kao korisnik!', '2012-07-06 10:10:58', 'm.s.'),
(20, 64, 'Ovaj komentar je izmjenio admin!', '2012-07-17 23:42:14', ''),
(95, 57, 'Super centar!!', '2014-07-01 11:56:39', 'dino'),
(96, 57, 'Najbolji centar u gradu i okolici!', '2014-07-01 11:56:52', 'dino'),
(97, 57, 'najgore iskustvo sa sportskim centrima ikad', '2014-07-01 11:58:17', 'ad');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `error_report`
--

CREATE TABLE IF NOT EXISTS `error_report` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_center` int(11) NOT NULL,
  `problem` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `suggestion` text,
  `user` char(20) NOT NULL,
  `seen` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`report_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Izbacivanje podataka za tablicu `error_report`
--

INSERT INTO `error_report` (`report_id`, `id_center`, `problem`, `time`, `suggestion`, `user`, `seen`, `status`) VALUES
(14, 56, 'description ', '2012-07-06 10:11:57', 'Prijavljujem grešku na svoj centar!', 'Dino', 0, 0),
(13, 55, 'name description ', '2012-07-06 09:51:25', 'Kakav je ovo razlog za upis?', 'admin', 0, 0),
(15, 71, 'vrijednosti nisu označene', '2012-07-09 19:01:44', 'Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra?!Iscrta mi, ali ne ispiše tekstualne upute za vožnju do centra', 'Dino', 0, 1),
(16, 64, 'description ', '2012-07-10 09:59:01', 'Nedostaje opis teretane!', 'Dino', 0, 1),
(66, 66, 'ret5ettq', '2014-05-23 23:01:25', '54uu', 'Dino', 0, 1);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `ratings`
--

CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `center_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=105 ;

--
-- Izbacivanje podataka za tablicu `ratings`
--

INSERT INTO `ratings` (`id`, `center_id`, `rating`) VALUES
(90, 55, 3),
(91, 56, 5),
(92, 66, 1),
(93, 57, 2),
(94, 71, 5),
(95, 57, 4),
(96, 71, 2),
(97, 71, 4),
(98, 67, 3),
(99, 67, 3),
(100, 67, 4),
(101, 67, 4),
(102, 67, 5),
(103, 67, 5),
(104, 57, 4);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `sport_center`
--

CREATE TABLE IF NOT EXISTS `sport_center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(60) NOT NULL,
  `street` text NOT NULL,
  `town` text NOT NULL,
  `post_num` int(11) DEFAULT NULL,
  `county` text,
  `country` text NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `added_by_user` char(20) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  `rating` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=112 ;

--
-- Izbacivanje podataka za tablicu `sport_center`
--

INSERT INTO `sport_center` (`id`, `name`, `street`, `town`, `post_num`, `county`, `country`, `lat`, `lng`, `added_by_user`, `date_added`, `description`, `rating`) VALUES
(55, 'Probni centar 1', 'Korzo 1', ' Rijeka', 51000, 'Primorsko-goranska', 'Croatia', 45.325756, 14.443939, 'admin', '2012-07-06 10:02:28', 'Centar je upisan kao proba! Izmjena zbog prijave greške! Nova aktivnost!', 3),
(56, 'Probni centar 2', 'Franje Tuđmana 12', ' Zadar', 23000, 'Zadarska', 'Croatia', 44.110996, 15.238756, 'm.s.', '2014-05-15 15:04:34', 'Ovaj probni centar unio sam kao korisnik!', 5),
(57, 'Apolon Zadar', 'Bana Josipa Jelačića 4', ' Zadar', 23000, 'Zadarska', 'Croatia', 44.116913, 15.236176, 'admin', '2014-06-16 12:17:55', 'hddhhhhhhhhhhhhhhhhhhhhhhhhhhhhgggggggggghhhhhhhhhhgggggghh\nd\nd\nd\nd\nd\nd\nd\nd', 3.33),
(58, 'Teniski centar Zadar', 'Sutomiška', ' Zadar', 0, 'Zadarska', 'Croatia', 44.129116, 15.221398, 'admin', '2012-07-06 13:05:22', '', 0),
(59, 'NK Zadar', 'Stadionska 2', ' Zadar', 0, 'Zadarska', 'Croatia', 44.112267, 15.248187, 'admin', '2012-07-06 13:07:19', '', 0),
(60, 'BK Zadar', 'Put Stanova 3', ' Zadar', 23000, 'Zadarska', 'Croatia', 44.115856, 15.244720, 'admin', '2014-07-01 11:52:50', 'Biciklistički klub Zadar', 0),
(61, 'Bandog Ultimate Fight klub', 'Put Pudarice', ' Zadar', 0, 'Zadarska', 'Croatia', 44.121849, 15.254743, 'admin', '2012-07-06 13:13:31', 'Svrha kluba je da se na jednom mjestu polaznicima ponudi mogućnost učenja  brazilskog jiu jitsua, hrvanja, thai boksa i boksa. U pitanju su sve vještine koje su potrebne za tzv. slobodnu borbu ili popularno nazvan "ultimate fight".', 0),
(62, 'Boksački klub Marijan Split', 'Papandopulova 11', ' Split', 0, 'Splitsko-dalmatinska', 'Croatia', 43.507572, 16.473185, 'admin', '2012-07-06 13:22:58', '', 0),
(63, 'Blue Gym Centar Kantrida', 'Istarska', ' Rijeka', 0, 'Primorsko-goranska', 'Croatia', 45.342319, 14.366719, 'admin', '2012-07-06 13:25:34', 'Centar je namjenjen fitnessu i rekreaciji građana, tjelesnoj pripremi sportaša i kineziterapij (liječenje pokretom). Sastoji se od prostora za tjelovježbu, sanitarnog prostora, kozmetičko-terapeutskog prostora, klupskog vitamin bara i trgovine dodacima prehrani.', 0),
(64, 'Cosmo Gym', 'Stube Marka Remsa 14', ' Rijeka', 0, 'Primorsko-goranska', 'Croatia', 45.334850, 14.435847, 'admin', '2012-07-06 13:29:30', 'Niske cijene za učenike i studente!', 0),
(65, 'Tenis klub Kvarner', 'Ede Jardasa 27', ' Rijeka', 0, 'Primorsko-goranska', 'Croatia', 45.345844, 14.370715, 'admin', '2012-07-06 13:32:17', '', 0),
(66, 'Teniski klub KOZALA', 'Petra Kobeka 11', ' Rijeka', 0, 'Primorsko-goranska', 'Croatia', 45.333832, 14.441765, 'admin', '2012-07-06 13:45:16', '', 1),
(67, 'Fitness centar HULK', 'Stupari', ' Viškovo', 0, 'Primorsko-goranska', 'Croatia', 45.365803, 14.394975, 'admin', '2012-07-16 14:51:15', '', 4),
(68, 'Plivački klub DUPIN', 'Vladimira Nazora', ' Biograd na Moru', 0, 'Zadarska', 'Croatia', 43.937050, 15.446053, 'admin', '2012-07-06 13:44:32', '', 0),
(69, 'SKIBOO sportska akademija', 'Milovana Kovačevića 3', ' Zagreb', 0, 'Zagreb', 'Croatia', 45.774136, 15.983892, 'admin', '2012-07-06 13:47:01', '', 0),
(70, 'Plivački klub PRIMORJE', 'Podkoludricu 2', ' Rijeka', 51216, 'Primorsko-goranska', 'Croatia', 45.340591, 14.373961, 'admin', '2012-07-06 13:55:19', '', 0),
(71, 'Teniski centar Concordia', 'Klanječka 37', ' Zagreb', 10000, 'Zagreb', 'Croatia', 45.806129, 15.936109, 'msj', '2014-05-15 13:20:26', 'Centar nudi otvorene (a zimi natkrivene) zemljane terene za tenis te zatvorene terene za badminton i squash (racquetball). \nU sklopu centra je cafe bar i restoran te trgovina s teniskim rekvizitima.', 3.67),
(72, 'Probni centar', 'Korzo', 'Rijeka', 51000, 'Primorje-Gorski Kotar County', 'Croatia', 45.326904, 14.442001, 'mate', '2013-09-11 17:21:30', '', 0),
(73, 'Bobojoga', 'Vukovarska ulica 58', '51000', 0, 'Rijeka', 'Croatia', 45.337513, 14.425786, 'proba', '2013-09-13 12:04:27', '', 0),
(111, 'asd', 'čšćuilgh', '', 0, '', '', 0.000000, 0.000000, '', '2014-05-21 12:19:34', 'LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL B', 0);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` char(20) NOT NULL,
  `password` char(32) NOT NULL,
  `name` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` char(40) NOT NULL,
  `phone` char(15) DEFAULT NULL,
  `mobile` char(15) NOT NULL,
  `date_added` varchar(20) NOT NULL,
  `last_login` varchar(20) NOT NULL,
  `activation_key` varchar(100) DEFAULT NULL,
  `verify` tinyint(1) NOT NULL DEFAULT '0',
  `rights` int(11) NOT NULL DEFAULT '0',
  `recover_pass` varchar(100) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Izbacivanje podataka za tablicu `users`
--

INSERT INTO `users` (`username`, `password`, `name`, `lastname`, `email`, `phone`, `mobile`, `date_added`, `last_login`, `activation_key`, `verify`, `rights`, `recover_pass`) VALUES
('admin', '343b1c4a3ea721b2d640fc8700db0f36', 'admin', 'adminovic', 'bikic.dino@gmail.com', '+38592213582', '+385922135827', '2012-03-06 15:36:19', '2014-08-14 12:07:48', '132111702018739405018059362311769845491621712693', 1, 1, '1519410526'),
('Dino', '75cc6e86c068577df0e6e09613b5d203', 'dino', 'bikic', 'nodiizlagune@gmail.com', '', '', '2014-03-28 19:21:50', '2014-07-01 11:55:58', NULL, 1, 0, ''),
('msj', '0e698a8ffc1a0af622c7b4db3cb750cc', 'M', 'Joler', 'jolerapps@gmail.com', '', '', '2012-07-09 18:27:45', '2012-07-09 18:36:31', NULL, 1, 0, ''),
('msj2', '4d5e2a885578299e5a5902ad295447c6', 'M', 'Joler', 'mjoler@gmail.com', '', '', '2012-07-09 19:16:30', '2012-07-09 19:31:33', NULL, 1, 0, ''),
('ad', 'efe6398127928f1b2e9ef3207fb82663', 'ado', 'ado', 'mail@mail.mail', NULL, '', '', '2014-08-31 13:13:12', NULL, 1, 1, ''),
('mate', '3e8fc8cbc8f7f62cc33e2b35143ae2f1', 'mate', 'stulina', 'mstulina@hotmail.com', '', '', '2013-09-02 21:35:50', '2013-09-12 03:13:22', NULL, 1, 0, ''),
('Proba', '7167c32d592ee7992fa2ec10bd97faee', 'm', 'j', 'mjoler@yahoo.com', '', '', '2013-09-13 02:33:15', '2013-09-13 11:53:33', NULL, 1, 0, '');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `user_blacklist`
--

CREATE TABLE IF NOT EXISTS `user_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Izbacivanje podataka za tablicu `user_blacklist`
--

