<?php
/* Geocodeing address string
 * Input: string,
 * Request method: GET,
 * Return request format: XML,
 * Result parsed and checked
 */
//$address = "korzo 1 rijeka croatia";
$address = preg_replace("/ž/", '$1z$2', $address);
$address = preg_replace("/Ž/", '$1z$2', $address);
$address = preg_replace("/š/", '$1s$2', $address);
$address = preg_replace("/Š/", '$1s$2', $address);
$address = preg_replace("/đ/", '$1d$2', $address);
$address = preg_replace("/Đ/", '$1d$2', $address);
$address = preg_replace("/ć/", '$1c$2', $address);
$address = preg_replace("/Ć/", '$1c$2', $address);
$address = preg_replace("/č/", '$1c$2', $address);
$address = preg_replace("/Č/", '$1c$2', $address);

$request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . urlencode($address) . "&sensor=false";

$xml = simplexml_load_file($request_url);
//var_dump($xml);
if (strcmp($xml->status, "OK") == 0) {
    // Format: Longitude, Latitude, Altitude
    $lat = $xml->result->geometry->location->lat;
    $lng = $xml->result->geometry->location->lng;
    $formatedAddress = $xml->result->formatted_address;
    $formatedAddress = preg_replace("/'/", '$1`$2', $formatedAddress);
    $formatedAddressSplit = explode(", ", $formatedAddress);
    $street = $formatedAddressSplit[0];
    $postNum = $formatedAddressSplit[1];
    $town = $formatedAddressSplit[2]; 
    $count = count($formatedAddressSplit);    
    if ($count == 4){
        $county = $xml->result->address_component[3]->long_name;
        $country=$formatedAddressSplit[3];
    }else if ($count == 5){
        $county = $formatedAddressSplit[3];
        $country=$formatedAddressSplit[4];
    }else{
        return false;
    }
    return true;
} else {
    // failure to geocode
    return false;
}
?>
