# README #

This is the back-end part of the mobile application SportsCenterFinder for Android. The main purpose of the back-end is to provide responses for API calls from the app. It is written in PHP, uses MySQL database and formats the API responses as JSON objects. 

### It implements: ###

* Search engine for finding sport centers and users
* Registration of new users (generating activation key and sending it to users email)
* Recovering lost password
* Logging in
* Fetching information about centers and users
* Generating image of the geolocation of a center via Google Maps api